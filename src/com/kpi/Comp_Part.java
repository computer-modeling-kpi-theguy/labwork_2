package com.kpi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Comp_Part implements SysInterface {

    private ArrayList<Task> queue;
    private double timeProcessing;
    private boolean isOpen;
    private Task currentTask;
    private Map<SysInterface,Double> nextParts;
    private double timeInWork;


    private String title;

    public Comp_Part(String title, double timeOfProcessing){
        queue = new ArrayList<>();
        this.timeProcessing = timeOfProcessing;
        this.title = title;
        isOpen = true;
        nextParts = new HashMap<>();
    }

    @Override
    public void addTaskInQueue(Task task){
        queue.add(task);
        if (currentTask == null)
            getNextTaskFromQueue();
    }

    public void incTimeWorking(){
        timeInWork += timeProcessing;
    }

    @Override
    public SysInterface movToNextPart() {
        SysInterface result = null;


        ArrayList<Map.Entry<SysInterface,Double>> entries = new ArrayList<>();
        entries.addAll(nextParts.entrySet());

        double rand = Math.random();
        double choice = 0;
        for (Map.Entry<SysInterface, Double> entry : entries) {
            choice += entry.getValue();
            if (choice > rand) {
                result = entry.getKey();
                break;
            }
        }

        if (result == null)
            result = entries.get(entries.size() - 1).getKey();
        return result;
    }

    @Override
    public Task getNextTaskFromPart() {
        return getCurrentTask();
    }

    @Override
    public void getNextTaskFromQueue() {
        if (currentTask == null && queue.size() > 0){
            currentTask = queue.get(0);
            currentTask.setTimeStarted(Comp_System.currentTime);
            queue.remove(0);
            incTimeWorking();
        }
        else{
            currentTask = null;
        }
    }

    @Override
    public double getTimeProcessing() {
        return timeProcessing;
    }

    @Override
    public double getTimeInWork() {
        return timeInWork;
    }

    public Task getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(Task currentTask) {
        this.currentTask = currentTask;
    }


    public Map<SysInterface, Double> getNextParts() {
        return nextParts;
    }

    public String getTitle() {

        return title;
    }
}
