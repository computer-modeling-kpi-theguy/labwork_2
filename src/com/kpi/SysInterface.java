package com.kpi;

public interface SysInterface {
    SysInterface movToNextPart();

    Task getNextTaskFromPart();

    void getNextTaskFromQueue();

    double getTimeProcessing();

    void addTaskInQueue(Task task);

    double getTimeInWork();
}
