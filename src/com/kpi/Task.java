package com.kpi;

public class Task {

    private static int count = 0;

    private int id;
    private double timeStarted;
    private double timeFinished;

    public Task(){
        count++;
        this.id = count;
    }

    public void setTimeStarted(double timeStarted) {
        this.timeStarted = timeStarted;
    }

    public double getTimeStarted() {
        return timeStarted;
    }

    public void setTimeFinished(double timeFinished) {
        this.timeFinished = timeFinished;
    }
}
