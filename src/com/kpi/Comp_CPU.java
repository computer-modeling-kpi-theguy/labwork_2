package com.kpi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Comp_CPU implements SysInterface {

    private ArrayList<Task> queue;
    private double timeProcessing;
    private boolean isOpen;
    private Task[] tasks;
    private Map<SysInterface,Double> nextParts;
    private String title = "Central Processing Unit";

    private double timeInWork;

    public Comp_CPU(double timeInProcessing, int cores, int numberOfTasks){
        this.timeProcessing = timeInProcessing;
        tasks = new Task[cores];
        queue = new ArrayList<>();
        nextParts = new HashMap<>();

        if (cores < numberOfTasks){
            for (int i = 0; i < cores; i++) {
                tasks[i] = new Task();
            }
            for (int i = 0; i < numberOfTasks - cores; i++) {
                queue.add(new Task());
            }
        }
        else{
            for (int i = 0; i < numberOfTasks; i++) {
                tasks[i] = new Task();
            }
        }
    }

    public int indexOfTask (Task task){
        int result = -1;
        for (int i = 0; i < tasks.length; i++) {
            if (tasks[i] != null && tasks[i].equals(task)) {
                result = i;
                break;
            }
        }
        return result;
    }

    public void incTimeInWork(){
        Task task = getNextTaskFromPart();
        timeInWork += task.getTimeStarted() + timeProcessing - Comp_System.currentTime;
    }

    @Override
    public SysInterface movToNextPart() {
        SysInterface result = null;

        ArrayList<Map.Entry<SysInterface,Double>> entries = new ArrayList<>();
        entries.addAll(nextParts.entrySet());

        if (entries.size() == 1)
            result = entries.get(0).getKey();
        else{
            double rand = Math.random();
            double choice = 0;
            for (Map.Entry<SysInterface, Double> entry : entries) {
                choice += entry.getValue();
                if (choice > rand) {
                    result = entry.getKey();
                    break;
                }
            }
        }

        if (result == null)
            result = entries.get(entries.size() - 1).getKey();
        return result;
    }

    @Override
    public Task getNextTaskFromPart() {
        Task result = null;

        for (int i = 0; i < tasks.length; i++) {
            if (tasks[i] != null){
                result = tasks[i];
                break;
            }
        }
        if(result != null){
            for (int i = 0; i < tasks.length; i++) {
                if(tasks[i] != null && tasks[i].getTimeStarted() < result.getTimeStarted()){
                    result = tasks[i];
                }
            }
        }
        return result;
    }

    @Override
    public void addTaskInQueue(Task task){
        queue.add(task);
        for (int i = 0; i < tasks.length; i++) {
            if(queue.size() == 0)
                break;
            if(tasks[i] == null)
                getNextTaskFromQueue();
        }
    }

    @Override
    public void getNextTaskFromQueue() {
        for (int i = 0; i < tasks.length; i++) {
            if(queue.size() == 0)
                break;
            if(tasks[i] == null){
                tasks[i] = queue.get(0);
                queue.remove(0);
                tasks[i].setTimeStarted(Comp_System.currentTime);
                incTimeInWork();
            }
        }
    }

    @Override
    public double getTimeProcessing() {
        return timeProcessing;
    }

    @Override
    public double getTimeInWork() {
        return timeInWork;
    }

    public Task[] getTasks() {
        return tasks;
    }

    public Map<SysInterface, Double> getNextParts() {
        return nextParts;
    }

    public String getTitle() {
        return title;
    }
}
