package com.kpi;

public class Comp_System {
    private static double finishingTime = 10000;

    public static double currentTime;
    private static double timeToNextTask;
    private static SysInterface nextCompPart;

    private Comp_CPU CPU;

    private Comp_Part NB;
    private Comp_Part OM;
    private Comp_Part GP;
    private Comp_Part SB;
    private Comp_Part VP;
    private Comp_Part IA;
    private Comp_Part DC;
    private Comp_Part AP;

    public Comp_System(int cores, int numberOfTasks){
        CPU = new Comp_CPU(2,cores,numberOfTasks);

        NB = new Comp_Part("North Bridge", 1);
        OM = new Comp_Part("Operating Memory", 6);
        GP = new Comp_Part("Graphical Processor",3);
        SB = new Comp_Part("South Bridge", 3);
        VP = new Comp_Part("Video Processor",20);
        IA = new Comp_Part("Internet Adapter", 150);
        DC = new Comp_Part("Disk Controller", 100);
        AP = new Comp_Part("Audio Processor", 15);

        initializeSystem();
    }

    private void initializeSystem(){
        CPU.getNextParts().put(NB, 1.0);

        NB.getNextParts().put(CPU, 0.4);
        NB.getNextParts().put(OM, 0.4);
        NB.getNextParts().put(SB, 0.16);
        NB.getNextParts().put(GP, 0.04);

        OM.getNextParts().put(NB, 1.0);

        GP.getNextParts().put(NB, 1.0);

        SB.getNextParts().put(VP, 0.1);
        SB.getNextParts().put(IA, 0.15);
        SB.getNextParts().put(DC, 0.15);
        SB.getNextParts().put(NB, 0.4);
        SB.getNextParts().put(AP, 0.2);

        VP.getNextParts().put(CPU, 1.0);
        IA.getNextParts().put(SB, 1.0);
        DC.getNextParts().put(SB, 1.0);
        AP.getNextParts().put(CPU, 1.0);
    }

    public void startSystem(){
        while(currentTime <= finishingTime){
            findNextPart();
            currentTime += timeToNextTask;
            computeNextTask(nextCompPart);
        }
    }

    private void findNextPart(){
        timeToNextTask = Integer.MAX_VALUE;
        double temp;

        temp = timeToFinishTask(CPU);
        if(temp >= 0 && temp < timeToNextTask){
            timeToNextTask = temp;
            nextCompPart = CPU;
        }

        temp = timeToFinishTask(NB);
        if(temp >= 0 && temp < timeToNextTask){
            timeToNextTask = temp;
            nextCompPart = NB;
        }

        temp = timeToFinishTask(OM);
        if(temp >= 0 && temp < timeToNextTask){
            timeToNextTask = temp;
            nextCompPart = OM;
        }

        temp = timeToFinishTask(GP);
        if(temp >= 0 && temp < timeToNextTask){
            timeToNextTask = temp;
            nextCompPart = GP;
        }

        temp = timeToFinishTask(SB);
        if(temp >= 0 && temp < timeToNextTask){
            timeToNextTask = temp;
            nextCompPart = SB;
        }

        temp = timeToFinishTask(VP);
        if(temp >= 0 && temp < timeToNextTask){
            timeToNextTask = temp;
            nextCompPart = VP;
        }

        temp = timeToFinishTask(IA);
        if(temp >= 0 && temp < timeToNextTask){
            timeToNextTask = temp;
            nextCompPart = IA;
        }

        temp = timeToFinishTask(DC);
        if(temp >= 0 && temp < timeToNextTask){
            timeToNextTask = temp;
            nextCompPart = DC;
        }

        temp = timeToFinishTask(AP);
        if(temp >= 0 && temp < timeToNextTask){
            timeToNextTask = temp;
            nextCompPart = AP;
        }

    }

    private double timeToFinishTask(SysInterface part){
        double result = -1;
        Task task = part.getNextTaskFromPart();
        if(task != null)
            result = task.getTimeStarted() + part.getTimeProcessing() - currentTime;
        return result;
    }

    private void computeNextTask(SysInterface part){
        Task task = part.getNextTaskFromPart();
        task.setTimeFinished(currentTime);

        if(part.equals(CPU)){
            int index = CPU.indexOfTask(task);
            CPU.getTasks()[index] = null;
        }
        else{
            Comp_Part comp_part = (Comp_Part)part;
            comp_part.setCurrentTask(null);
        }
        part.getNextTaskFromQueue();

        SysInterface nextPart = part.movToNextPart();
        nextPart.addTaskInQueue(task);
    }

    public void getStatistics(){
        System.out.println("System worked that time: "+currentTime);
        System.out.println(CPU.getTitle()+" worked that time: "+CPU.getTimeInWork()+" , "+timeInPercent(CPU)+" %");
        System.out.println(NB.getTitle()+" worked that time: "+NB.getTimeInWork()+" , "+timeInPercent(NB)+" %");
        System.out.println(OM.getTitle()+" worked that time: "+OM.getTimeInWork()+" , "+timeInPercent(OM)+" %");
        System.out.println(GP.getTitle()+" worked that time: "+GP.getTimeInWork()+" , "+timeInPercent(GP)+" %");
        System.out.println(SB.getTitle()+" worked that time: "+SB.getTimeInWork()+" , "+timeInPercent(SB)+" %");
        System.out.println(VP.getTitle()+" worked that time: "+VP.getTimeInWork()+" , "+timeInPercent(VP)+" %");
        System.out.println(IA.getTitle()+" worked that time: "+IA.getTimeInWork()+" , "+timeInPercent(IA)+" %");
        System.out.println(DC.getTitle()+" worked that time: "+DC.getTimeInWork()+" , "+timeInPercent(DC)+" %");
        System.out.println(AP.getTitle()+" worked that time: "+AP.getTimeInWork()+" , "+timeInPercent(AP)+" %");
    }

    private double timeInPercent(SysInterface part){
        double result = part.getTimeInWork()/currentTime*10000;
        int res = (int)result;
        return (double)res/100;
    }
}
