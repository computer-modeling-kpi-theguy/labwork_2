Laboratory work #2 for Computer Modelling.

Purpose for the work: To study and practice of creating theoretical Petri net, using the Computer components.
Programming language: Java
Variant of the used computer parts:
CPU (Central Processing Unit), NB (North Bridge), OM (Operating Memory), GP (Graphical Processor), SB (South Bridge), VP (Video Processor), IA (Internet Adapter), DC (Disk Controller), AP (Audio Processor).

Task for the Laboratory Work: Create the working model of the Computer system using the algorithm of the Petri net, place inside of it some task, and calculate the characteristics of working model by simulation of the time.

Controling of the program:
Starting the program enables the work of all the system, that corresponds to this model (described in the picture at the repository root).

Program output as result that values: System time work, time of work of every Computer part, and it's percentage accordance.